package com.company.convert.temperature.enums;

import com.company.utils.Unit;

public enum TempeatureName implements Unit {
    CELSIUS("градус Цельсия") {
        @Override
        public double toKelvin(double in) {
            return in + 273.15;
        }

        @Override
        public double fromKelvin(double kelvin) {
            return kelvin - 273.15;
        }
    },
    FAHRENHEIT("градус Фаренгейта") {
        @Override
        public double toKelvin(double in) {
            return (in + 459.67) * (5.0 / 9.0);
        }

        @Override
        public double fromKelvin(double kelvin) {
            return (kelvin * (9.0 / 5.0)) - 459.67;
        }
    },
    KELVIN("Кельвин") {
        @Override
        public double toKelvin(double in) {
            return in;
        }

        @Override
        public double fromKelvin(double kelvin) {
            return kelvin;
        }
    },
    REAUMUR("градус Реомюра") {
        @Override
        public double toKelvin(double in) {
            return (in / 0.8) + 273.15;
        }

        @Override
        public double fromKelvin(double kelvin) {
            return (kelvin - 273.15) * 0.8;
        }
    };

    /**
     * Функция переводит значение в Кельвины
     * @param in искомое значение
     */
    public abstract double toKelvin(double in);

    /**
     * Функция переводит значение из Кельвинов
     * @param kelvin искомое значение
     */
    public abstract double fromKelvin(double kelvin);

    private final String name;

    TempeatureName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
