package com.company.convert.temperature;

import com.company.convert.temperature.enums.TempeatureName;
import com.company.utils.Converter;

public class TemperatureConverter extends Converter<TempeatureName> {

    public TemperatureConverter(TempeatureName sourceCurrency, TempeatureName targetCurrency) {
        super(sourceCurrency, targetCurrency);
    }



    @Override
    public double convert(double sourceAmount) {
        TempeatureName sourceTemperature = getSource();
        TempeatureName targetTemperature = getTarget();
        double inKelvin = sourceTemperature.toKelvin(sourceAmount);
        return targetTemperature.fromKelvin(inKelvin);
    }
}
