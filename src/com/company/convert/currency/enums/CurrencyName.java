package com.company.convert.currency.enums;

import com.company.utils.Unit;

public enum CurrencyName implements Unit {
    USD("Доллар"),
    RUB("Рубль"),
    EUR("Евро"),
    JPY("Иена"),
    GBP("Фунт");


    private final String name;

    @Override
    public String getName() {
        return name;
    }

    CurrencyName(String name) {
        this.name = name;
    }
}
