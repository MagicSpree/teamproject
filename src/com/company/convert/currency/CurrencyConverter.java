package com.company.convert.currency;

import com.company.convert.currency.enums.CurrencyName;
import com.company.utils.ConverterWithMap;

public class CurrencyConverter extends ConverterWithMap<CurrencyName> {

    /**
     * Конструктор класса CurrencyConverter
     *
     * @param sourceCurrency  Название искомой единицы измерения
     * @param targetCurrency  Название конечной единицы измерения
     */
    public CurrencyConverter(CurrencyName sourceCurrency, CurrencyName targetCurrency) {
        super(sourceCurrency,targetCurrency);
        addCurrencies();
    }

    /**
     * Функция добавляет Enum объекты и их значения в Map
     */
    private void addCurrencies() {
        addUnit(CurrencyName.USD, 1.0);
        addUnit(CurrencyName.RUB, 72.78);
        addUnit(CurrencyName.EUR, 0.88);
        addUnit(CurrencyName.JPY, 114.84);
        addUnit(CurrencyName.GBP, 0.74);
    }

    /**
     * Функция конвертирует значение
     *
     * @param sourceAmount значение, которе хотим перевести
     * @return конечное конвертируемое значение
     */
    @Override
    public double convert(double sourceAmount) {
        CurrencyName sourceCurrency = getSource();
        CurrencyName targetCurrency = getTarget();
        double rateToUSD = getUnitRate(sourceCurrency);
        double targetToUSD = getUnitRate(targetCurrency);
        return (sourceAmount / rateToUSD) * targetToUSD;
    }
}
