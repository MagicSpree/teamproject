package com.company.convert.information;

import com.company.convert.information.enums.InformationName;
import com.company.utils.ConverterWithMap;

public class InformationConverter extends ConverterWithMap<InformationName> {

    public InformationConverter(InformationName sourceInformation, InformationName targetInformation) {
        super(sourceInformation,targetInformation);
        addInformation();
    }

    private void addInformation() {              //хранит степень двойки (содержит 2^X бит)
        addUnit(InformationName.BIT, 0.0);  //содержит 2^0 бит
        addUnit(InformationName.BYTE, 3.0); //содержит 2^3 бит
        addUnit(InformationName.KB, 13.0);  //содержит 2^13 бит
        addUnit(InformationName.MB, 23.0);  //содержит 2^23 бит
        addUnit(InformationName.GB, 33.0);  //содержит 2^33 бит
    }

    private double twoPow(double n){     //ф-ция возведения 2 в степень
        double result = 1;
        for (int i=0; i<n; i++) {
            result *= 2;
        }
        return result;
    }

    @Override
    public double convert(double sourceAmount) {
        InformationName sourceInfo = getSource();
        InformationName targetInfo = getTarget();
        double source = getUnitRate(sourceInfo);
        double target = getUnitRate(targetInfo);
        return sourceAmount*twoPow(source-target); //кол-во * 2^X
    }
}
