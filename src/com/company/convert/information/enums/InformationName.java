package com.company.convert.information.enums;

import com.company.utils.Unit;

public enum InformationName implements Unit {
    BIT("Бит"),
    BYTE("Байт"),
    KB("Килобайт"),
    MB("Мегабайт"),
    GB("Гигабайт");

    private final String name;

    public String getName() {
        return name;
    }

    InformationName(String name) {
        this.name = name;
    }
}
