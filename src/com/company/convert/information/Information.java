package com.company.convert.information;
import com.company.convert.information.enums.InformationName;

public class Information {

    InformationName informationName;
    double rate;

    public Information(InformationName informationName, double rate) {
        this.informationName = informationName;
        this.rate = rate;
    }

    public InformationName getInformationName() {
        return informationName;
    }

    public double getRate() {
        return rate;
    }


}