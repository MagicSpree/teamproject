package com.company.utils;

import java.util.HashMap;

public abstract class Converter<T extends Unit> implements Convert {
    private final T source;
    private final T target;
//    private final HashMap<T, Double> units = new HashMap<>();

    protected Converter(T source, T target) {
        this.source = source;
        this.target = target;
    }

    /**
      Функция добавляет Enum объекты и их значения в Map

      @param enumElement Enum объект
      @param rate        значение Enum объекта

    public void addUnit(T enumElement, double rate) {
        units.put(enumElement, rate);
    }

    /**
     * Функция позволяется получить значение из Map по ключу
     *
     * @param unitsName Объект "Ключ"
     * @return значение Enum объекта

    public double getUnitRate(T unitsName) {
        return units.get(unitsName);
    }
*/

    public T getSource() {
        return source;
    }

    public T getTarget() {
        return target;
    }
}
